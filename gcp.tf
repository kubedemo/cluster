# # set up a GKE cluster

# requires GOOGLE_CREDENTIALS environment variable with GCP service account key (contents of the JSON file)
# service account requires the following roles in IAM: 
# - Kubernetes Engine Admin
# - Kubernetes Engine Service Agent
# - Storage Object Admin
# - DNS Administrator

variable "project" {
  description = "GCP project ID"
}
variable "location" {
  description = "GKE cluster location (zone or region for a regional cluster)"
  default = "us-east1-c"
}
variable "machine_type" {
  default = "g1-small"      # 1 shared vCPU, 1.7 GB memory
}
variable "disk_type" {
  default = "pd-standard"   # standard == hdd
}
variable "disk_size_gb" {
  default = 20
}
variable "max_node_count" {
  default = 10
}
variable "preemptible" {
  default = true
}
variable "dns_zone" {
}
variable "dns_name" {
}
# dns ip is set to 0.0.0.0 initially for running terraform apply -target=google_container_node_pool.main
# when GKE contains an ingress, dns ip is set using terraform apply -target=google_dns_record_set.main -var dns_ip=$INGRESS_IP
variable "dns_ip" {
  default = "0.0.0.0"
}

terraform {
  backend "gcs" { }
}

# use google-beta for trying new features such as istio
provider "google-beta" {
  project = "${var.project}"
}

resource "google_container_cluster" "main" {
  provider = "google-beta"
  name = "${terraform.workspace}"           # workspace name is used as a name of the cluster
  location = "${var.location}"

  maintenance_policy {
    daily_maintenance_window {
      start_time = "01:00"
    }
  }

  # dedicated stackdriver monitoring/logging module for kubernetes
  logging_service = "logging.googleapis.com/kubernetes"
  monitoring_service = "monitoring.googleapis.com/kubernetes"

  # don't use basic auth and certificates, use IAM service accounts instead
  master_auth = {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  ip_allocation_policy {
    # VPC-native networking for Pods. Avoids using route quotas and treats pods as VMs for GCP services
    use_ip_aliases = true       
    # separate subnet for each cluster due to limited alias count of 5
    # each cluster needs 2 aliases for pods and services, so default subnet can only have 2 VPC-native clusters
    create_subnetwork = true
  }

  addons_config {
    http_load_balancing {
      disabled = false
    }
    kubernetes_dashboard {
      disabled = true
    }
    # istio_config {
    #   disabled = false
    # }
    # network_policy_config {
    #   disabled = false        # network policy for master 
    # }
  }
  # network_policy {
  #   enabled = true            # network policy for nodes, requires network_policy_config
  #   provider = "CALICO"       # consumes about 300 mCPU for each node if enabled
  # }

  # default node pool does not allow advanced options (e.g. autoscaling, management)
  initial_node_count = 1
  remove_default_node_pool = true
}

resource "google_container_node_pool" "main" {
  provider = "google-beta"
  name = "main"
  cluster = "${google_container_cluster.main.name}"
  location = "${var.location}"
  node_config {
    preemptible = "${var.preemptible}"
    machine_type = "${var.machine_type}"
    disk_type = "${var.disk_type}"
    disk_size_gb = "${var.disk_size_gb}"
  }

  # for regional clusters this number of nodes is replicated in 3 different zones
  node_count = 1
  autoscaling {
    min_node_count = 1
    max_node_count = "${var.max_node_count}"
  }

  # let Google team fix errors and upgrade Kuberenetes versions
  management {
    auto_repair = true
    auto_upgrade = true
  }
}

resource "google_dns_record_set" "main" {
  provider = "google-beta"
  managed_zone = "${var.dns_zone}"
  name = "${substr("${var.dns_name}", -1, 1) == "." ? "${var.dns_name}" : "${var.dns_name}."}"
  type = "A"
  ttl  = 60
  rrdatas = ["${var.dns_ip}"]
}