## Google Kubernetes Engine cluster for testing GitLab continuous deployment

- GKE configured on every commit using terraform, gcloud and kubectl
- Custom branches configure the staging cluster (ingress DNS: https://staging.kubedemo.in)
- Master branch configures the main production cluster (ingress DNS: https://kubedemo.in)
- Node.js example app deployed from a separate GitLab repository: https://gitlab.com/kubedemo/interface