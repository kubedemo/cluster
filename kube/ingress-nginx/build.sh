#!/bin/sh
# this script generates a versioned kustomize build that can be applied using kubectl apply -f build.yml
# (fetches the most recent container and manifest versions from github)
# build.yml should be checked into repository for versioned CI/CD deployments and rollbacks
cd $(dirname $0)
kubectl kustomize > build.yml