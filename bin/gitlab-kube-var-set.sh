#!/bin/bash
# sets kubernetes environment variables for other gitlab projects, see https://docs.gitlab.com/ee/api/project_level_variables.html

usage() { 
    echo "Usage: $0         \
        -g <GITLAB_TOKEN>   \
        -n <NAMESPACE>      \
        -p <PROJECT>        \
        -e <ENVIRONMENT>    \
        -s <KUBE_SERVER>    \
        -c <KUBE_CA>        \
        -t <KUBE_TOKEN>" 1>&2
        exit 1
}

while getopts "g:n:p:e:s:c:t:" o; do
    case "$o" in
        g) GITLAB_TOKEN=$OPTARG     ;;
        n) NAMESPACE=$OPTARG        ;;
        p) PROJECT=$OPTARG          ;;
        e) ENVIRONMENT=$OPTARG      ;;
        s) KUBE_SERVER=$OPTARG      ;;
        c) KUBE_CA=$OPTARG          ;;
        t) KUBE_TOKEN=$OPTARG       ;;
        h) usage                    ;;
    esac
done

if [ -z "$GITLAB_TOKEN" ]   \
|| [ -z "$NAMESPACE" ]      \
|| [ -z "$PROJECT" ]        \
|| [ -z "$ENVIRONMENT" ]    \
|| [ -z "$KUBE_SERVER" ]    \
|| [ -z "$KUBE_CA" ]        \
|| [ -z "$KUBE_TOKEN" ]; then
    usage
else
    set_var() {
        VAR_KEY=$1
        VAR_VAL=$2
        VARS_URL="https://gitlab.com/api/v4/projects/${NAMESPACE}%2F${PROJECT}/variables"
        echo "GET $VARS_URL/$VAR_KEY"
        curl -is --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$VARS_URL/$VAR_KEY" | grep '404 Not Found'
        if [ $? -eq 0 ]; then
            echo "POST $VARS_URL --form key=$VAR_KEY"
            curl -is --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --request POST "$VARS_URL" --form "key=$VAR_KEY" \
                --form "value=$VAR_VAL" | grep '201 Created'
        else
            echo "PUT $VARS_URL/$VAR_KEY"
            curl -is --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --request PUT "$VARS_URL/$VAR_KEY" \
                --form "value=$VAR_VAL" | grep '200 OK'
        fi
        if [ $? -ne 0 ]; then
            echo "Could not create variable $VAR_KEY at $VARS_URL"
            exit 1
        fi
    }
    set_var KUBE_SERVER_$ENVIRONMENT $KUBE_SERVER
    set_var KUBE_CA_$ENVIRONMENT $KUBE_CA
    set_var KUBE_TOKEN_$ENVIRONMENT $KUBE_TOKEN
fi
