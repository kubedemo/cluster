// set kubernetes environment variables in other gitlab repositories
// sets 3 environment variables in the following format:
// - $KUBE_SERVER_<ENVIRONMENT>
// - $KUBE_CA_<ENVIRONMENT>
// - $KUBE_TOKEN_<ENVIRONMENT>
// run gitlab-kubevar.go -h for flag details

package main

import (
    "github.com/jessevdk/go-flags"
    "github.com/xanzy/go-gitlab"
    "log"
)

type kubeFlags struct {
    GITLAB_TOKEN    string    `short:"g" required:"true"  description:"Gitlab access token for editing project CI/CD variables"`
    NAMESPACE       string    `short:"n" required:"true"  description:"Gitlab project namespace (group or username)"`
    PROJECT         string    `short:"p" required:"true"  description:"Gitlab project name"`
    ENVIRONMENT     string    `short:"e" required:"true"  description:"Environment name which will be appended to kubernetes vars (e.g. staging/production)"`
    KUBE_SERVER     string    `short:"s" required:"true"  description:"Kubernetes API address"`
    KUBE_CA         string    `short:"c" required:"true"  description:"Kubernetes service account certificate authority (base64 encoded)"`
    KUBE_TOKEN      string    `short:"t" required:"true"  description:"Kubernetes service account token (base64 encoded)"`
    UNSET           bool      `short:"u" description:"Extra flag for removing old variables"`
}

func setVars(opts *kubeFlags) {

    git := gitlab.NewClient(nil, opts.GITLAB_TOKEN)
    
    project_path := opts.NAMESPACE + "/" + opts.PROJECT
    vopts := &gitlab.ListProjectVariablesOptions{}

    vars_old, _, err := git.ProjectVariables.ListVariables(project_path, vopts)
    if err != nil {
        log.Fatal(err)
    }

    vars_new := map[string]string{
        "KUBE_SERVER_" + opts.ENVIRONMENT: opts.KUBE_SERVER,
        "KUBE_CA_"     + opts.ENVIRONMENT: opts.KUBE_CA,
        "KUBE_TOKEN_"  + opts.ENVIRONMENT: opts.KUBE_TOKEN,
    }

    for k, v := range vars_new {
    
        // delete old variables if -u flag is set
        if opts.UNSET {
            git.ProjectVariables.RemoveVariable(project_path, k)
            continue
        }

        present := false
        for _, old_var_struct := range vars_old {
            if old_var_struct.Key == k {
                present = true
                break
            }
        }

        if present {
            vopts := &gitlab.UpdateProjectVariableOptions {
                Value: &v,
            }
            _, _, err := git.ProjectVariables.UpdateVariable(project_path, k, vopts)
            if err != nil {
                log.Fatal(err)
            }
        } else {
            vopts := &gitlab.CreateProjectVariableOptions {
                Key: &k,
                Value: &v,
            }
            _, _, err := git.ProjectVariables.CreateVariable(project_path, vopts)
            if err != nil {
                log.Fatal(err)
            }
        }
    }
}

func main() {
    var opts kubeFlags

    _, err := flags.Parse(&opts)
    if err != nil {
        log.Fatal(err)
    }

    setVars(&opts)
}
