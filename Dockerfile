# # for configuring GKE using terraform, gcloud and kubectl commands
FROM golang:1.13.0 AS build_go
COPY bin /go/src/gitlab-kubevar
RUN go get /go/src/gitlab-kubevar
RUN go build /go/src/gitlab-kubevar

FROM hashicorp/terraform:0.11.14 AS build_terraform
FROM google/cloud-sdk:248.0.0-alpine
WORKDIR /opt/app
RUN gcloud components install kubectl -q
COPY --from=build_terraform /bin/terraform /bin/terraform
COPY --from=build_go /go/bin/gitlab-kubevar /opt/app/bin/gitlab-kubevar
COPY . .
RUN terraform init -backend=false